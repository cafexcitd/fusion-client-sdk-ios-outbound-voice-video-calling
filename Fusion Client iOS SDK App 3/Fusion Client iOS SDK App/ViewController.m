//
//  ViewController.m
//  Fusion Client iOS SDK App
//

#import "ViewController.h"
#import "ConfigViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // assign acbuc object and init local video stream
    self.acbuc = [ConfigViewController getACBUC];
    self.acbuc.phone.previewView = self.local;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

////////////////////////////////////////////
//
// UI Methods

-(IBAction)dial:(id)sender
{
    [self logMessage: [NSString stringWithFormat:@"User dialed %@", self.number.text]];
    ACBClientCall *call = [self.acbuc.phone
                           createCallToAddress:self.number.text
                           audio:self.audio.on
                           video:self.video.on
                           delegate:self];
    
    call.videoView = self.remote;
}

-(IBAction)hangup:(id)sender
{
    [self logMessage:@"User hungup"];
}

-(IBAction)toggleMedia:(id)sender
{
    [self logMessage:[NSString stringWithFormat:
                      @"User video: %d, voice: %d",
                      self.video.on, self.audio.on] ];
}

-(IBAction)userDoneEnteringText:(id)sender
{
    [sender resignFirstResponder];
}

-(void)logMessage:(NSString*)message
{
    self.log.text = [NSString stringWithFormat:@"%@\n%@", message, self.log.text];
    NSLog(@":::%@", message);
}

-(NSString*) statusToString: (ACBClientCallStatus)status
{
    NSArray *names = [NSArray arrayWithObjects:
                      @"ACBClientCallStatusSetup",
                      @"ACBClientCallStatusAlerting",
                      @"ACBClientCallStatusRinging",
                      @"ACBClientCallStatusMediaPending",
                      @"ACBClientCallStatusInCall",
                      @"ACBClientCallStatusTimedOut",
                      @"ACBClientCallStatusBusy",
                      @"ACBClientCallStatusNotFound",
                      @"ACBClientCallStatusError",
                      @"ACBClientCallStatusEnded", nil];
    return [names objectAtIndex:status];
}

////////////////////////////////////////////
//
// ACBClientCallDelegate methods

-(void) call:(ACBClientCall *)call didChangeRemoteDisplayName:(NSString *)name
{
    [self logMessage:[NSString stringWithFormat: @"didChangeRemoteDisplayName: %@", name]];
}

-(void) call:(ACBClientCall *)call didChangeStatus:(ACBClientCallStatus)status
{
    NSString *statusString = [self statusToString:status];
    [self logMessage:[NSString stringWithFormat: @"didChangeStatus: %@", statusString]];
}

-(void) call:(ACBClientCall *)call didReceiveCallFailure:(NSString *)message
{
    [self logMessage:[NSString stringWithFormat:@"didReceiveCallFailure: %@", message]];
}

-(void) call:(ACBClientCall *)call didReceiveDialFailure:(NSString *)message
{
    [self logMessage:[NSString stringWithFormat:@"didReceiveDialFailure: %@", message]];
}

-(void) callDidAddLocalMediaStream:(ACBClientCall *)call
{
    [self logMessage:@"callDidAddLocalMediaStream"];
}

-(void) callDidAddRemoteMediaStream:(ACBClientCall *)call
{
    [self logMessage:@"callDidAddRemoteMediaStream"];
}

-(void) callDidReceiveMediaChangeRequest:(ACBClientCall *)call
{
    [self logMessage:@"callDidReceiveMediaChangeRequest"];
}

- (void) call:(ACBClientCall*)call didReportInboundQualityChange:(NSUInteger)inboundQuality
{
    [self logMessage:[NSString stringWithFormat:@"calldidReportInboundQualityChange: %d", inboundQuality]];
}


@end
