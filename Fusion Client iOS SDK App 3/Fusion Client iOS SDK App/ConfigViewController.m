//
//  ConfigViewController.m
//  Fusion Client iOS SDK App
//

#import "ConfigViewController.h"

@interface ConfigViewController ()

@end

@implementation ConfigViewController

static ACBUC *acbuc;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.password.secureTextEntry = true;
	self.started = false;
}

+ (ACBUC *) getACBUC
{
    return acbuc;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/////////////////////////////////////////////////////
//
// UI methods

-(void) setNotStarted
{
    self.started = false;
    [self.startStopButton setTitle:@"Start" forState:UIControlStateNormal];
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void) toggleStart:(UIButton *)sender
{
    if (!self.started) {
        // if we've not started, then do so...
        NSString *username = self.username.text;
        NSString *password = self.password.text;
        
        // get the session id...
        NSString *urlString = [NSString stringWithFormat:@"http://192.168.50.157/id.php?reset&username=%@&password=%@", username, password];
        NSURL *url = [NSURL URLWithString:urlString];
        NSString *sessionID = [NSString stringWithContentsOfURL:url encoding:NSASCIIStringEncoding error:nil];
        
        // now init the with the session id
        acbuc = [ACBUC ucWithConfiguration:sessionID delegate:self];
        [acbuc setNetworkReachable:true];
        [acbuc startSession];
        
        NSLog(@"::Session: %@", sessionID);
    }
    else {
        // to log out: 1. notify the server-side component of your app to DELETE
        // the session on the Fusion Web Gateway, and 2. close the connection down
        // from the client application by sending the ACBUC the stopSession message
        NSURL *url = [NSURL URLWithString:@"http://192.168.50.157/logout.php"];
        [NSData dataWithContentsOfURL:url];
        [self.acbuc stopSession];
        
        // since there is no callback for stopSession - go ahead and update UI
        [self setNotStarted];
    }
}

-(IBAction)userDoneEnteringText:(id)sender
{
    NSLog(@"::userDoneEnteringText");
    [sender resignFirstResponder];
}

/////////////////////////////////////////////////////
//
// ACBUCDelegate methods

-(void)ucDidStartSession:(ACBUC *)uc
{
    NSLog(@"::ucDidStartSession");
    self.started = true;
    [self.startStopButton setTitle:@"Stop" forState:UIControlStateNormal];
    self.view.backgroundColor = [UIColor colorWithRed:0.5 green:0.8 blue:0.5 alpha:1.0];
}

-(void)ucDidFailToStartSession:(ACBUC *)uc
{
    NSLog(@"::ucDidFailToStartSession");
}

-(void)ucDidLoseConnection:(ACBUC *)uc
{
    NSLog(@"::ucDidLoseConnection");
    [self setNotStarted];
}

-(void)ucDidReceiveSystemFailure:(ACBUC *)uc
{
    NSLog(@"::ucDidReceiveSystemFailure");
    [self setNotStarted];
}

// optional delegate methods

- (void) uc:(ACBUC*)uc willRetryConnectionNumber:(NSUInteger)attemptNumber in:(NSTimeInterval)delay
{
    NSLog(@"::willRetryConnectionNumber");
}

- (void) ucDidReestablishConnection:(ACBUC*)uc
{
    NSLog(@"::willRetryConnectionNumber");
    [self setStarted:true];
}

@end
