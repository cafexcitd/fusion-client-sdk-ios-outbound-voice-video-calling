//
//  ConfigViewController.h
//  Fusion Client iOS SDK App
//
//

#import <UIKit/UIKit.h>
#import <ACBClientSDK/ACBUC.h>

@interface ConfigViewController : UIViewController <ACBUCDelegate>

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *startStopButton;
@property (weak, nonatomic) ACBUC *acbuc;

@property (atomic) bool started;

-(IBAction)toggleStart:(UIButton*)sender;

+(ACBUC *) getACBUC;

@end
